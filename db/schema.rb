# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170204152516) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "comments", force: :cascade do |t|
    t.string   "author_name"
    t.string   "author_email"
    t.boolean  "is_published"
    t.text     "content"
    t.integer  "user_id"
    t.integer  "parent_id"
    t.integer  "facility_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "commentable_type"
    t.integer  "commentable_id"
    t.index ["commentable_type", "commentable_id"], name: "index_comments_on_commentable_type_and_commentable_id", using: :btree
  end

  create_table "contact_relations", force: :cascade do |t|
    t.integer  "contact_id"
    t.integer  "order"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "contacted_type"
    t.integer  "contacted_id"
    t.index ["contact_id"], name: "index_contact_relations_on_contact_id", using: :btree
    t.index ["contacted_type", "contacted_id"], name: "index_contact_relations_on_contacted_type_and_contacted_id", using: :btree
  end

  create_table "contacts", force: :cascade do |t|
    t.string   "title"
    t.text     "content"
    t.boolean  "is_published"
    t.integer  "facility_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "departments", force: :cascade do |t|
    t.string   "slug"
    t.string   "title"
    t.text     "content"
    t.boolean  "is_published"
    t.integer  "header_img_id"
    t.integer  "facility_id"
    t.integer  "chief_id"
    t.integer  "parent_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "districts", force: :cascade do |t|
    t.integer  "emias_id"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "employee_relations", force: :cascade do |t|
    t.integer  "employee_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "employable_type"
    t.integer  "employable_id"
    t.index ["employable_type", "employable_id"], name: "index_employee_relations_on_employable_type_and_employable_id", using: :btree
    t.index ["employee_id"], name: "index_employee_relations_on_employee_id", using: :btree
  end

  create_table "employees", force: :cascade do |t|
    t.string   "slug"
    t.string   "name"
    t.boolean  "is_administration"
    t.integer  "facility_id"
    t.integer  "mediafile_id"
    t.json     "data"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "job_id"
    t.string   "position_id"
  end

  create_table "facilities", force: :cascade do |t|
    t.string   "slug"
    t.string   "name"
    t.integer  "director_id"
    t.integer  "parent_id"
    t.json     "address"
    t.json     "data"
    t.json     "hosts"
    t.boolean  "is_published",              default: false
    t.integer  "favicon_id"
    t.integer  "license_id"
    t.integer  "logo_id"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.string   "code"
    t.string   "point"
    t.integer  "district_id"
    t.integer  "facility_type_id"
    t.boolean  "has_pediatric_department"
    t.boolean  "has_emergency_aid_station"
    t.boolean  "has_pharmacy"
    t.string   "short_name"
  end

  create_table "facility_types", force: :cascade do |t|
    t.integer  "emias_id"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "links", force: :cascade do |t|
    t.text     "text"
    t.string   "url"
    t.boolean  "is_target_blank"
    t.integer  "mediafile_id"
    t.integer  "menu_id"
    t.integer  "facility_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "linked_type"
    t.integer  "linked_id"
    t.index ["facility_id"], name: "index_links_on_facility_id", using: :btree
    t.index ["linked_type", "linked_id"], name: "index_links_on_linked_type_and_linked_id", using: :btree
    t.index ["mediafile_id"], name: "index_links_on_mediafile_id", using: :btree
    t.index ["menu_id"], name: "index_links_on_menu_id", using: :btree
  end

  create_table "localities", force: :cascade do |t|
    t.string   "street"
    t.string   "building"
    t.integer  "number"
    t.integer  "therapist_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "mediafiles", force: :cascade do |t|
    t.text     "file_data"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "mediable_type"
    t.integer  "mediable_id"
    t.string   "description"
    t.index ["mediable_type", "mediable_id"], name: "index_mediafiles_on_mediable_type_and_mediable_id", using: :btree
  end

  create_table "menus", force: :cascade do |t|
    t.string   "title"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "news", force: :cascade do |t|
    t.string   "slug"
    t.string   "title"
    t.text     "content"
    t.boolean  "is_published"
    t.integer  "thumbnail_id"
    t.integer  "header_img_id"
    t.integer  "facility_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "pages", force: :cascade do |t|
    t.string   "slug"
    t.string   "title"
    t.text     "content"
    t.boolean  "is_published"
    t.integer  "header_img_id"
    t.integer  "thumbnail_id"
    t.integer  "facility_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "positions", force: :cascade do |t|
    t.integer  "emias_id"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "prices", force: :cascade do |t|
    t.string   "title"
    t.text     "price"
    t.boolean  "is_published"
    t.json     "data"
    t.integer  "facility_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "priced_type"
    t.integer  "priced_id"
    t.index ["priced_type", "priced_id"], name: "index_prices_on_priced_type_and_priced_id", using: :btree
  end

  create_table "schedule_rules", force: :cascade do |t|
    t.string   "action"
    t.string   "parity"
    t.text     "description"
    t.date     "start_date"
    t.date     "end_date"
    t.time     "start_time"
    t.time     "end_time"
    t.json     "weekdays"
    t.integer  "facility_id"
    t.integer  "schedule_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "schedules", force: :cascade do |t|
    t.string   "room"
    t.integer  "weight"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "scheduled_type"
    t.integer  "scheduled_id"
    t.index ["scheduled_type", "scheduled_id"], name: "index_schedules_on_scheduled_type_and_scheduled_id", using: :btree
  end

  create_table "services", force: :cascade do |t|
    t.string   "slug"
    t.string   "title"
    t.text     "content"
    t.boolean  "is_published"
    t.boolean  "is_free"
    t.integer  "header_img_id"
    t.integer  "department_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "slides", force: :cascade do |t|
    t.string   "title"
    t.text     "text"
    t.boolean  "is_published"
    t.integer  "order"
    t.integer  "mediafile_id"
    t.integer  "facility_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "subway_stations", force: :cascade do |t|
    t.integer  "emias_id"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "surveys", force: :cascade do |t|
    t.text     "question"
    t.json     "data"
    t.integer  "facility_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "name"
    t.string   "role"
    t.integer  "facility_id"
    t.integer  "avatar_id"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "vacancies", force: :cascade do |t|
    t.string   "slug"
    t.string   "title"
    t.integer  "amount"
    t.boolean  "is_published"
    t.text     "content"
    t.integer  "facility_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_foreign_key "slides", "mediafiles"
end
