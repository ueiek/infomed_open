class CreateFacilityTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :facility_types do |t|
      t.integer :emias_id
      t.string  :name

      t.timestamps
    end
  end
end
