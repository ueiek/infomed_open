class ChangePolymorphicAssociationToWorkspaceModel < ActiveRecord::Migration[5.0]
  def change
    remove_reference :workspaces, :model, polymorphic: true, index: true
    add_reference    :workspaces, :employable, polymorphic: true, index: true
  end
end
