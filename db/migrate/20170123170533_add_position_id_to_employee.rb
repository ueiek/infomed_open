class AddPositionIdToEmployee < ActiveRecord::Migration[5.0]
  def change
    add_column :employees, :position_id, :string
  end
end
