class AddIsPublishedDefaultColumnsToFacilityModel < ActiveRecord::Migration[5.0]
  def change
    change_column :facilities, :is_published, :boolean, default: false
  end
end
