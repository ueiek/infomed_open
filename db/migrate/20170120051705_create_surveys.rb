# frozen_string_literal: true
class CreateSurveys < ActiveRecord::Migration[5.0]
  def change
    create_table :surveys do |t|
      t.text :question
      t.json :data
      t.integer :facility_id, foreign_key: true, inedx: true

      t.timestamps
    end
  end
end
