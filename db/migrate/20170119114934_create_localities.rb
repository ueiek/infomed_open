# frozen_string_literal: true
class CreateLocalities < ActiveRecord::Migration[5.0]
  def change
    create_table :localities do |t|
      t.string   :street
      t.string   :building
      t.integer  :number
      t.integer  :therapist_id

      t.timestamps
    end
  end
end
