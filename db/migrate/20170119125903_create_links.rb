# frozen_string_literal: true
class CreateLinks < ActiveRecord::Migration[5.0]
  def change
    create_table :links do |t|
      t.text    :text
      t.string  :url
      t.boolean :is_target_blank
      t.integer :mediafile_id, foreign_key: true, index: true
      t.integer :menu_id, foreign_key: true, index: true
      t.integer :facility_id, foreign_key: true, index: true

      t.timestamps
    end
    add_reference :links, :model, polymorphic: true, index: true
  end
end
