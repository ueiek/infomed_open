# frozen_string_literal: true
class CreateClinics < ActiveRecord::Migration[5.0]
  def change
    create_table :clinics do |t|
      t.string  :slug
      t.string  :title
      t.integer :chief_id
      t.integer :parent_id
      t.json    :address
      t.json    :data
      t.json    :hosts
      t.boolean :is_published
      t.integer :favicon_id
      t.integer :license_id
      t.integer :logo_id
      t.timestamps
    end
  end
end
