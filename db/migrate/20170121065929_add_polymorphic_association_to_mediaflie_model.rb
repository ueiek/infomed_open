class AddPolymorphicAssociationToMediaflieModel < ActiveRecord::Migration[5.0]
  def change
    add_reference :mediafiles,  :mediable, polymorphic: true, index: true
  end
end
