# frozen_string_literal: true
class CreateContactRelations < ActiveRecord::Migration[5.0]
  def change
    create_table :contact_relations do |t|
      t.integer :model_id,   index: true, foreign_key: true
      t.integer :contact_id, index: true, foreign_key: true
      t.string  :model_name, index: true, foreign_key: true
      t.integer :order

      t.timestamps
    end
  end
end
