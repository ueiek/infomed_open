class CreatePositions < ActiveRecord::Migration[5.0]
  def change
    create_table :positions do |t|
      t.integer :emias_id, unique: true
      t.string  :name, unique: true

      t.timestamps
    end
  end
end
