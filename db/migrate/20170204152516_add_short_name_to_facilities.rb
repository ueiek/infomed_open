class AddShortNameToFacilities < ActiveRecord::Migration[5.0]
  def change
    add_column :facilities, :short_name, :string
  end
end
