class CreateSchedules < ActiveRecord::Migration[5.0]
  def change
    create_table :schedules do |t|
      t.string  :room
      t.integer :weight

      t.timestamps
    end
    add_reference :schedules, :scheduled, polymorphic: true, index: true
  end
end
