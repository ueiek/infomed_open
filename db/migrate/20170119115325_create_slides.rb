# frozen_string_literal: true
class CreateSlides < ActiveRecord::Migration[5.0]
  def change
    create_table :slides do |t|
      t.string     :title
      t.text       :text
      t.boolean    :is_published
      t.integer    :order
      t.integer    :mediafile_id
      t.integer    :facility_id

      t.timestamps
    end
  end
end
