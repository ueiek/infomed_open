class RenameClinicModelToFacility < ActiveRecord::Migration[5.0]
  def self.up
    rename_table :clinics, :facilities
  end

  def self.down
    rename_table :facilities, :clinics
  end

end
