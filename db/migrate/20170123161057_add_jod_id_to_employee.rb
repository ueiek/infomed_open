class AddJodIdToEmployee < ActiveRecord::Migration[5.0]
  def change
    add_column :employees, :job_id, :string, unique: true
  end
end
