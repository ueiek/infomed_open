class ChangesInContactRelation < ActiveRecord::Migration[5.0]
  def change
    remove_column :contact_relations, :model_name
    remove_column :contact_relations, :model_id
    add_reference :contact_relations, :contacted, polymorphic: true, index: true
  end
end
