# frozen_string_literal: true
class CreateNews < ActiveRecord::Migration[5.0]
  def change
    create_table :news do |t|
      t.string  :slug
      t.string  :title
      t.text    :content
      t.boolean :is_published

      t.integer :thumbnail_id, foreign_key: true, inedx: true
      t.integer :header_img_id, foreign_key: true, inedx: true
      t.integer :facility_id, foreign_key: true, inedx: true

      t.timestamps
    end
  end
end
