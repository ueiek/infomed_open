# frozen_string_literal: true
class ChangesInPageModel < ActiveRecord::Migration[5.0]
  def change
    drop_table :pages
    create_table :pages do |t|
      t.string  :slug
      t.string  :title
      t.text    :content
      t.boolean :is_published

      t.integer :header_img_id, foreign_key: true, inedx: true
      t.integer :thumbnail_id, foreign_key: true, inedx: true
      t.integer :facility_id, foreign_key: true, inedx: true

      t.timestamps
    end

  end
end
