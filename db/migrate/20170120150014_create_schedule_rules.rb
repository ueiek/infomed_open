class CreateScheduleRules < ActiveRecord::Migration[5.0]
  def change
    create_table :schedule_rules do |t|
      t.string :action
      t.string :parity
      t.text :description
      t.date :start_date
      t.date :end_date
      t.time :start_time
      t.time :end_time
      t.json :weekdays

      t.integer :facility_id, foreign_key: true, inedx: true
      t.integer :schedule_id, foreign_key: true, inedx: true

      t.timestamps
    end
  end
end
