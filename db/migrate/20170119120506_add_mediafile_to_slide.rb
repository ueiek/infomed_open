# frozen_string_literal: true
class AddMediafileToSlide < ActiveRecord::Migration[5.0]
  def change
    add_foreign_key :slides, :mediafiles
  end
end
