class CreateVacancies < ActiveRecord::Migration[5.0]
  def change
    create_table :vacancies do |t|
      t.string  :slug
      t.string  :title
      t.integer :amount
      t.boolean :is_published
      t.text    :content

      t.integer :facility_id, foreign_key: true, inedx: true

      t.timestamps
    end
  end
end
