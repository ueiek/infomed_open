# frozen_string_literal: true
class CreatePrices < ActiveRecord::Migration[5.0]
  def change
    create_table :prices do |t|
      t.string  :title
      t.text    :price
      t.boolean :is_published
      t.json    :data

      t.integer :facility_id, foreign_key: true, inedx: true
      t.timestamps
    end
    add_reference :prices, :priced, polymorphic: true, index: true
  end
end
