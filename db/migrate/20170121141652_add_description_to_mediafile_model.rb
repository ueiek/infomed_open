class AddDescriptionToMediafileModel < ActiveRecord::Migration[5.0]
  def change
    add_column :mediafiles, :description, :string
  end
end
