class ChangePolymorphicAssociationToLinkModel < ActiveRecord::Migration[5.0]
  def change
    remove_reference :links, :model, polymorphic: true, index: true
    add_reference :links,  :linked, polymorphic: true, index: true
  end
end
