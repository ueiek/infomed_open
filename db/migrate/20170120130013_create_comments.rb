class CreateComments < ActiveRecord::Migration[5.0]
  def change
    create_table :comments do |t|
      t.string  :author_name
      t.string  :author_email
      t.boolean :is_published
      t.text    :content

      t.integer :user_id, foreign_key: true, inedx: true
      t.integer :parent_id, foreign_key: true, inedx: true
      t.integer :facility_id, foreign_key: true, inedx: true

      t.timestamps
    end
    add_reference :comments, :commentable, polymorphic: true, index: true
  end
end
