# frozen_string_literal: true
class CreateWorkspaces < ActiveRecord::Migration[5.0]
  def change
    create_table :workspaces do |t|
      t.integer :employee_id, index: true, foreign_key: true

      t.timestamps
    end
    add_reference :workspaces, :model, polymorphic: true, index: true
  end
end
