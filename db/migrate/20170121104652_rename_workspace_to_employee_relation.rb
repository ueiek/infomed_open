class RenameWorkspaceToEmployeeRelation < ActiveRecord::Migration[5.0]
  def self.up
    rename_table :workspaces, :employee_relations
  end

  def self.down
    rename_table :employee_relations, :workspaces
  end
end
