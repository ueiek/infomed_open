class AddEmiasColumnsToFacilityModel < ActiveRecord::Migration[5.0]
  def change
    rename_column :facilities, :title, :name
    rename_column :facilities, :chief_id, :director_id
    add_column    :facilities, :code, :string
    add_column    :facilities, :point, :string
    add_column    :facilities, :district_id, :integer, index: true, foregin_key: true
    add_column    :facilities, :facility_type_id, :integer, index: true, foregin_key: true
    add_column    :facilities, :has_pediatric_department, :boolean
    add_column    :facilities, :has_emergency_aid_station, :boolean
    add_column    :facilities, :has_pharmacy, :boolean

  end
end
