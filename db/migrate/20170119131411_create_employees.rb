# frozen_string_literal: true
class CreateEmployees < ActiveRecord::Migration[5.0]
  def change
    create_table :employees do |t|
      t.string  :slug
      t.string  :name
      t.boolean :is_administration
      t.integer :facility_id, foreign_key: true, inedx: true
      t.integer :mediafile_id, foreign_key: true, inedx: true
      t.json    :data

      t.timestamps
    end
  end
end
