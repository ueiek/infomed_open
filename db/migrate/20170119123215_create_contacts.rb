# frozen_string_literal: true
class CreateContacts < ActiveRecord::Migration[5.0]
  def change
    create_table :contacts do |t|
      t.string  :title
      t.text    :content
      t.boolean :is_published
      t.integer :facility_id

      t.timestamps
    end
  end
end
