# frozen_string_literal: true
class CreateMediafiles < ActiveRecord::Migration[5.0]
  def change
    create_table :mediafiles do |t|
      t.text :file_data

      t.timestamps
    end
  end
end
