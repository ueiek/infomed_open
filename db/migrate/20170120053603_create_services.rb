# frozen_string_literal: true
class CreateServices < ActiveRecord::Migration[5.0]
  def change
    create_table :services do |t|
      t.string  :slug
      t.string  :title
      t.text    :content
      t.boolean :is_published
      t.boolean :is_free

      t.integer :header_img_id, foreign_key: true, inedx: true
      t.integer :department_id, foreign_key: true, inedx: true
      t.timestamps
    end
  end
end
