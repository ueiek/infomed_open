# frozen_string_literal: true
# == Schema Information
#
# Table name: prices
#
#  id           :integer          not null, primary key
#  title        :string
#  price        :text
#  is_published :boolean
#  data         :json
#  facility_id  :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  priced_type  :string
#  priced_id    :integer
#
# Indexes
#
#  index_prices_on_priced_type_and_priced_id  (priced_type,priced_id)
#

# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Price, type: :model do
  it { should have_db_column(:title) }
  it { should have_db_column(:price) }
  it { should have_db_column(:is_published) }
  it { should have_db_column(:data) }
  it { should have_db_column(:facility_id) }
  it { should have_db_column(:priced_id) }
  it { should have_db_column(:priced_type) }

  it { should belong_to(:facility) }
  it { should belong_to(:priced) }
end
