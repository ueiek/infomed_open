# frozen_string_literal: true
# == Schema Information
#
# Table name: links
#
#  id              :integer          not null, primary key
#  text            :text
#  url             :string
#  is_target_blank :boolean
#  mediafile_id    :integer
#  menu_id         :integer
#  facility_id     :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  linked_type     :string
#  linked_id       :integer
#
# Indexes
#
#  index_links_on_facility_id                (facility_id)
#  index_links_on_linked_type_and_linked_id  (linked_type,linked_id)
#  index_links_on_mediafile_id               (mediafile_id)
#  index_links_on_menu_id                    (menu_id)
#

# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Link, type: :model do
  it { should have_db_column(:url) }
  it { should have_db_column(:text) }
  it { should have_db_column(:menu_id) }
  it { should have_db_column(:facility_id) }
  it { should have_db_column(:is_target_blank) }
  it { should have_db_column(:linked_type) }
  it { should have_db_column(:linked_id) }

  it { should belong_to(:mediafile) }
  it { should belong_to(:facility) }
  it { should belong_to(:menu) }
  it { should belong_to(:linked) }
end
