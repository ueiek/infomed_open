# frozen_string_literal: true
# == Schema Information
#
# Table name: departments
#
#  id            :integer          not null, primary key
#  slug          :string
#  title         :string
#  content       :text
#  is_published  :boolean
#  header_img_id :integer
#  facility_id   :integer
#  chief_id      :integer
#  parent_id     :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Department, type: :model do
  it { should have_db_column(:title) }
  it { should have_db_column(:slug) }
  it { should have_db_column(:content) }
  it { should have_db_column(:is_published) }
  it { should have_db_column(:header_img_id) }
  it { should have_db_column(:facility_id) }
  it { should have_db_column(:chief_id) }
  it { should have_db_column(:parent_id) }

  it { should have_many(:contacts) }
  it { should have_many(:contact_relations) }
  it { should have_many(:branches) }
  it { should have_many(:prices) }
  it { should have_many(:comments) }
  it { should belong_to(:facility) }
  it { should belong_to(:chief) }
  it { should belong_to(:parent) }
  it { should have_many(:employee_relations) }
  it { should have_many(:worked_employees) }
end
