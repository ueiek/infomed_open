# frozen_string_literal: true
# == Schema Information
#
# Table name: localities
#
#  id           :integer          not null, primary key
#  street       :string
#  building     :string
#  number       :integer
#  therapist_id :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Locality, type: :model do
  it { should have_db_column(:street) }
  it { should have_db_column(:building) }
  it { should have_db_column(:number) }
  it { should have_db_column(:therapist_id) }

  it { should belong_to(:therapist) }
end
