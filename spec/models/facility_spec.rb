# frozen_string_literal: true
# == Schema Information
#
# Table name: facilities
#
#  id                        :integer          not null, primary key
#  slug                      :string
#  name                      :string
#  director_id               :integer
#  parent_id                 :integer
#  address                   :json
#  data                      :json
#  hosts                     :json
#  is_published              :boolean          default("false")
#  favicon_id                :integer
#  license_id                :integer
#  logo_id                   :integer
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  code                      :string
#  point                     :string
#  district_id               :integer
#  facility_type_id          :integer
#  has_pediatric_department  :boolean
#  has_emergency_aid_station :boolean
#  has_pharmacy              :boolean
#  short_name                :string
#

# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Facility, type: :model do
  it { should have_db_column(:name) }
  it { should have_db_column(:short_name) }
  it { should have_db_column(:slug) }
  it { should have_db_column(:director_id) }
  it { should have_db_column(:parent_id) }
  it { should have_db_column(:address) }
  it { should have_db_column(:data) }
  it { should have_db_column(:hosts) }
  it { should have_db_column(:is_published) }
  it { should have_db_column(:favicon_id) }
  it { should have_db_column(:license_id) }
  it { should have_db_column(:logo_id) }
  it { should have_db_column(:code) }
  it { should have_db_column(:point) }
  it { should have_db_column(:district_id) }
  it { should have_db_column(:has_pediatric_department) }
  it { should have_db_column(:has_emergency_aid_station) }
  it { should have_db_column(:has_pharmacy) }

  it { should belong_to(:director) }
  it { should belong_to(:parent) }
  it { should belong_to(:favicon) }
  it { should belong_to(:license) }
  it { should belong_to(:logo) }

  it { should have_many(:branches) }
  it { should have_many(:comments) }
  it { should have_many(:departments) }
  it { should have_many(:employees) }
  it { should have_many(:news) }
  it { should have_many(:pages) }
  it { should have_many(:slides) }
  it { should have_many(:schedule_rules) }
  it { should have_many(:surveys) }
  it { should have_many(:users) }
  it { should have_many(:vacancies) }
  it { should have_many(:prices) }
  it { should have_many(:contacts) }
  it { should have_many(:contact_relations) }

  it { should have_many(:employee_relations) }
  it { should have_many(:worked_employees) }

  it { should validate_presence_of(:slug) }
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:address) }

  it 'is published false by default' do
    facility = build(:facility)
    expect(facility.is_published).to eql(false)
  end

  it 'should take short_name before create' do
    facility = build(:facility)
    expect(facility.is_published).to eql(false)
  end

  describe '#get_schedule_json' do
    it 'returns the json vim facility code' do
      facility = build(:facility)
      expect(facility.send(:get_schedule_json,
                           facility.code)['result']['lpuId'])
        .to eql(facility.code.to_i)
    end
  end

  describe '#fill_position_via_schedule_json' do
    it 'create all uniq position' do
      facility = build(:facility)
      position = facility.fill_position_via_schedule_json
      expect(position.length).to eql(Position.count)
    end
    it 'return nill if code is wrong' do
      facility = build(:facility, code: 'wrong')
      expect(facility.fill_position_via_schedule_json).to eql(nil)
    end
  end

  describe '#fill_employees_via_schedule_json' do
    it 'cannot create emplyees without position ' do
      facility = create(:facility)
      employees = facility.fill_employees_via_schedule_json
      expect(employees.length).to_not eql(Employee.count)
    end
    it 'can create emplyees only with exist position ' do
      facility = build(:facility)
      facility.fill_position_via_schedule_json
      employees = facility.fill_employees_via_schedule_json
      expect(employees.length).to_not eql(Employee.count)
    end
    it 'return nill if code is wrong' do
      facility = build(:facility, code: 'wrong')
      expect(facility.fill_employees_via_schedule_json).to eql(nil)
    end
  end
end
