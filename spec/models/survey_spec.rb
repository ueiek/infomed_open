# frozen_string_literal: true
# == Schema Information
#
# Table name: surveys
#
#  id          :integer          not null, primary key
#  question    :text
#  data        :json
#  facility_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Survey, type: :model do
  it { should have_db_column(:facility_id) }
  it { should have_db_column(:question) }
  it { should have_db_column(:data) }

  it { should belong_to(:facility) }
end
