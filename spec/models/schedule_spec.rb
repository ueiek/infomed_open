# == Schema Information
#
# Table name: schedules
#
#  id             :integer          not null, primary key
#  room           :string
#  weight         :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  scheduled_type :string
#  scheduled_id   :integer
#
# Indexes
#
#  index_schedules_on_scheduled_type_and_scheduled_id  (scheduled_type,scheduled_id)
#

require 'rails_helper'

RSpec.describe Schedule, type: :model do
  it { should have_db_column(:weight) }
  it { should have_db_column(:room) }

  it { should belong_to(:scheduled) }
  it { should have_many(:schedule_rules) }
end
