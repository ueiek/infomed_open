# == Schema Information
#
# Table name: positions
#
#  id         :integer          not null, primary key
#  emias_id   :integer
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Position, type: :model do
  it { should have_db_column(:emias_id) }
  it { should have_db_column(:name) }

  it { should have_many(:employees) }
end
