# == Schema Information
#
# Table name: vacancies
#
#  id           :integer          not null, primary key
#  slug         :string
#  title        :string
#  amount       :integer
#  is_published :boolean
#  content      :text
#  facility_id  :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

require 'rails_helper'

RSpec.describe Vacancy, type: :model do
  it { should have_db_column(:facility_id) }
  it { should have_db_column(:title) }
  it { should have_db_column(:slug) }
  it { should have_db_column(:amount) }
  it { should have_db_column(:content) }
  it { should have_db_column(:is_published) }

  it { should belong_to(:facility) }
end
