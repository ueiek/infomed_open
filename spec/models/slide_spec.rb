# frozen_string_literal: true
# == Schema Information
#
# Table name: slides
#
#  id           :integer          not null, primary key
#  title        :string
#  text         :text
#  is_published :boolean
#  order        :integer
#  mediafile_id :integer
#  facility_id  :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Slide, type: :model do
  it { should have_db_column(:title) }
  it { should have_db_column(:text) }
  it { should have_db_column(:is_published) }
  it { should have_db_column(:order) }
  it { should have_db_column(:mediafile_id) }
  it { should have_db_column(:facility_id) }

  it { should belong_to(:facility)}
  it { should belong_to(:mediafile) }
end
