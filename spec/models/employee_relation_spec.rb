# frozen_string_literal: true
# == Schema Information
#
# Table name: employee_relations
#
#  id              :integer          not null, primary key
#  employee_id     :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  employable_type :string
#  employable_id   :integer
#
# Indexes
#
#  index_employee_relations_on_employable_type_and_employable_id  (employable_type,employable_id)
#  index_employee_relations_on_employee_id                        (employee_id)
#

# frozen_string_literal: true
require 'rails_helper'

RSpec.describe EmployeeRelation, type: :model do
  it { should have_db_column(:employee_id) }
  it { should have_db_column(:employable_id) }
  it { should have_db_column(:employable_type) }

  it { should belong_to(:employable) }
  it { should belong_to(:employee) }
end
