# frozen_string_literal: true
# == Schema Information
#
# Table name: mediafiles
#
#  id            :integer          not null, primary key
#  file_data     :text
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  mediable_type :string
#  mediable_id   :integer
#  description   :string
#
# Indexes
#
#  index_mediafiles_on_mediable_type_and_mediable_id  (mediable_type,mediable_id)
#

# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Mediafile, type: :model do
  it { should have_db_column(:file_data) }
  it { should have_db_column(:description) }
  it { should have_db_column(:mediable_type) }
  it { should have_db_column(:mediable_id) }

  it { should belong_to(:mediable) }
end
