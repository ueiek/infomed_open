# == Schema Information
#
# Table name: subway_stations
#
#  id         :integer          not null, primary key
#  emias_id   :integer
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe SubwayStation, type: :model do
  it { should have_db_column(:emias_id) }
  it { should have_db_column(:name) }
end
