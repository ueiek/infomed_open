# frozen_string_literal: true
# == Schema Information
#
# Table name: news
#
#  id            :integer          not null, primary key
#  slug          :string
#  title         :string
#  content       :text
#  is_published  :boolean
#  thumbnail_id  :integer
#  header_img_id :integer
#  facility_id   :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

# frozen_string_literal: true
require 'rails_helper'

RSpec.describe News, type: :model do
  it { should have_db_column(:title) }
  it { should have_db_column(:slug) }
  it { should have_db_column(:content) }
  it { should have_db_column(:is_published) }
  it { should have_db_column(:thumbnail_id) }
  it { should have_db_column(:header_img_id) }
  it { should have_db_column(:facility_id) }

  it { should belong_to(:facility) }
  it { should belong_to(:header_img) }
  it { should belong_to(:thumbnail) }
  it { should have_many(:photos) }
  it { should have_many(:comments) }
end
