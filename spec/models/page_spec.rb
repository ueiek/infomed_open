# == Schema Information
#
# Table name: pages
#
#  id            :integer          not null, primary key
#  slug          :string
#  title         :string
#  content       :text
#  is_published  :boolean
#  header_img_id :integer
#  thumbnail_id  :integer
#  facility_id   :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

require 'rails_helper'

RSpec.describe Page, type: :model do
  it { should have_db_column(:slug) }
  it { should have_db_column(:title) }
  it { should have_db_column(:content) }
  it { should have_db_column(:is_published) }
  it { should have_db_column(:header_img_id) }
  it { should have_db_column(:thumbnail_id) }
  it { should have_db_column(:facility_id) }
  it { should validate_presence_of(:slug) }
  it { should have_many(:photos) }
  it { should belong_to(:facility) }
  it { should belong_to(:thumbnail) }
  it { should belong_to(:header_img) }
end
