# == Schema Information
#
# Table name: comments
#
#  id               :integer          not null, primary key
#  author_name      :string
#  author_email     :string
#  is_published     :boolean
#  content          :text
#  user_id          :integer
#  parent_id        :integer
#  facility_id      :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  commentable_type :string
#  commentable_id   :integer
#
# Indexes
#
#  index_comments_on_commentable_type_and_commentable_id  (commentable_type,commentable_id)
#

require 'rails_helper'

RSpec.describe Comment, type: :model do
  it { should have_db_column(:content) }
  it { should have_db_column(:facility_id) }
  it { should have_db_column(:author_name) }
  it { should have_db_column(:author_email) }
  it { should have_db_column(:parent_id) }
  it { should have_db_column(:user_id) }
  it { should have_db_column(:is_published) }

  it { should belong_to(:commentable) }
  it { should belong_to(:facility) }
  it { should belong_to(:parent) }
  it { should have_many(:children) }
  it { should belong_to(:user) }

end
