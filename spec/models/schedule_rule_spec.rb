# == Schema Information
#
# Table name: schedule_rules
#
#  id          :integer          not null, primary key
#  action      :string
#  parity      :string
#  description :text
#  start_date  :date
#  end_date    :date
#  start_time  :time
#  end_time    :time
#  weekdays    :json
#  facility_id :integer
#  schedule_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe ScheduleRule, type: :model do
  it { should have_db_column(:action) }
  it { should have_db_column(:parity) }
  it { should have_db_column(:weekdays) }
  it { should have_db_column(:start_date) }
  it { should have_db_column(:end_date) }
  it { should have_db_column(:start_time) }
  it { should have_db_column(:end_time) }
  it { should have_db_column(:description) }

  it { should have_db_column(:schedule_id) }
  it { should have_db_column(:facility_id) }

  it { should belong_to(:schedule) }
  it { should belong_to(:facility) }
end
