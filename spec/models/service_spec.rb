# frozen_string_literal: true
# == Schema Information
#
# Table name: services
#
#  id            :integer          not null, primary key
#  slug          :string
#  title         :string
#  content       :text
#  is_published  :boolean
#  is_free       :boolean
#  header_img_id :integer
#  department_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Service, type: :model do
  it { should have_db_column(:slug) }
  it { should have_db_column(:title) }
  it { should have_db_column(:content) }
  it { should have_db_column(:is_published) }
  it { should have_db_column(:is_free) }
  it { should have_db_column(:header_img_id) }
  it { should have_db_column(:department_id) }

  it { should have_many(:contacts) }
  it { should have_many(:contact_relations) }
  it { should have_many(:prices) }
  it { should have_many(:contacts) }
  it { should have_many(:photos) }
  it { should belong_to(:department) }
  it { should belong_to(:header_img) }
end
