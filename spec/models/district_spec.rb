# == Schema Information
#
# Table name: districts
#
#  id         :integer          not null, primary key
#  emias_id   :integer
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe District, type: :model do
  it { should have_db_column(:emias_id) }
  it { should have_db_column(:name) }
end
