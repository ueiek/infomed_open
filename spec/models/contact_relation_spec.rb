# frozen_string_literal: true
# == Schema Information
#
# Table name: contact_relations
#
#  id             :integer          not null, primary key
#  contact_id     :integer
#  order          :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  contacted_type :string
#  contacted_id   :integer
#
# Indexes
#
#  index_contact_relations_on_contact_id                       (contact_id)
#  index_contact_relations_on_contacted_type_and_contacted_id  (contacted_type,contacted_id)
#

# frozen_string_literal: true
require 'rails_helper'

RSpec.describe ContactRelation, type: :model do
  it { should have_db_column(:order) }
  it { should have_db_column(:contact_id) }

  it { should belong_to(:contact) }
  it { should belong_to(:contacted) }
end
