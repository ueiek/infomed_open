# frozen_string_literal: true
# == Schema Information
#
# Table name: menus
#
#  id          :integer          not null, primary key
#  title       :string
#  description :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Menu, type: :model do
  it { should have_db_column(:title) }
  it { should have_db_column(:description) }

  it { should have_many(:links) }
end
