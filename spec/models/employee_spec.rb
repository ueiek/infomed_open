# frozen_string_literal: true
# == Schema Information
#
# Table name: employees
#
#  id                :integer          not null, primary key
#  slug              :string
#  name              :string
#  is_administration :boolean
#  facility_id       :integer
#  mediafile_id      :integer
#  data              :json
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  job_id            :string
#  position_id       :string
#

# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Employee, type: :model do
  it { should have_db_column(:slug) }
  it { should have_db_column(:name) }
  it { should have_db_column(:is_administration) }
  it { should have_db_column(:facility_id) }
  it { should have_db_column(:mediafile_id) }
  it { should have_db_column(:data) }
  it { should have_db_column(:job_id) }
  it { should have_db_column(:position_id) }

  it { should belong_to(:facility) }
  it { should belong_to(:portrait) }
  it { should have_many(:employee_relations) }
  it { should have_many(:contacts) }
  it { should have_many(:contact_relations) }
  it { should have_many(:facilities) }
  it { should have_many(:departments) }
  it { should have_many(:comments) }

  describe 'Data' do
    it 'if data structure is broken must raise error' do
      employee = build(:employee)
      employee.data = 3
      expect { employee.save }
        .to raise_exception ArgumentError, 'data structure must be a hash'
    end
  end
end
