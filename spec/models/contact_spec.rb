# frozen_string_literal: true
# == Schema Information
#
# Table name: contacts
#
#  id           :integer          not null, primary key
#  title        :string
#  content      :text
#  is_published :boolean
#  facility_id  :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Contact, type: :model do
  it { should have_db_column(:facility_id) }
  it { should have_db_column(:is_published) }
  it { should have_db_column(:content) }
  it { should have_db_column(:title) }

  it { should belong_to(:facility) }
  it { should have_one(:schedule) }
  it { should have_many(:facilities) }
  it { should have_many(:departments) }
  it { should have_many(:employees) }
  it { should have_many(:services) }
  it { should have_many(:contact_relations) }
end
