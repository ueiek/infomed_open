require 'rails_helper'

RSpec.describe "facilities/edit", type: :view do
  before(:each) do
    @facility = build(:facility)
  end

  skip 'renders the edit facility form' do
    render

    assert_select 'form[action=?][method=?]', facility_path(@facility), 'post' do
    end
  end
end
