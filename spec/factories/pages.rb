# == Schema Information
#
# Table name: pages
#
#  id            :integer          not null, primary key
#  slug          :string
#  title         :string
#  content       :text
#  is_published  :boolean
#  header_img_id :integer
#  thumbnail_id  :integer
#  facility_id   :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

FactoryGirl.define do
  factory :page do
    slug 	'first_page'
    title 	'first_title'
    content     'blunk'
    is_published true
    association :clinic, factory: :clinic
  end
end
