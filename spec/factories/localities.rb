# frozen_string_literal: true
# == Schema Information
#
# Table name: localities
#
#  id           :integer          not null, primary key
#  street       :string
#  building     :string
#  number       :integer
#  therapist_id :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

# frozen_string_literal: true
FactoryGirl.define do
  factory :locality do
  end
end
