# frozen_string_literal: true
# == Schema Information
#
# Table name: services
#
#  id            :integer          not null, primary key
#  slug          :string
#  title         :string
#  content       :text
#  is_published  :boolean
#  is_free       :boolean
#  header_img_id :integer
#  department_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

# frozen_string_literal: true
FactoryGirl.define do
  factory :service do
  end
end
