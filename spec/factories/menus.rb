# frozen_string_literal: true
# == Schema Information
#
# Table name: menus
#
#  id          :integer          not null, primary key
#  title       :string
#  description :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

# frozen_string_literal: true
FactoryGirl.define do
  factory :menu do
  end
end
