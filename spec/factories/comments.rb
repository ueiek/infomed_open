# == Schema Information
#
# Table name: comments
#
#  id               :integer          not null, primary key
#  author_name      :string
#  author_email     :string
#  is_published     :boolean
#  content          :text
#  user_id          :integer
#  parent_id        :integer
#  facility_id      :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  commentable_type :string
#  commentable_id   :integer
#
# Indexes
#
#  index_comments_on_commentable_type_and_commentable_id  (commentable_type,commentable_id)
#

FactoryGirl.define do
  factory :comment do
    
  end
end
