# frozen_string_literal: true
# == Schema Information
#
# Table name: prices
#
#  id           :integer          not null, primary key
#  title        :string
#  price        :text
#  is_published :boolean
#  data         :json
#  facility_id  :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  priced_type  :string
#  priced_id    :integer
#
# Indexes
#
#  index_prices_on_priced_type_and_priced_id  (priced_type,priced_id)
#

# frozen_string_literal: true
FactoryGirl.define do
  factory :price do
  end
end
