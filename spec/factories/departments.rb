# frozen_string_literal: true
# == Schema Information
#
# Table name: departments
#
#  id            :integer          not null, primary key
#  slug          :string
#  title         :string
#  content       :text
#  is_published  :boolean
#  header_img_id :integer
#  facility_id   :integer
#  chief_id      :integer
#  parent_id     :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

# frozen_string_literal: true
FactoryGirl.define do
  factory :department do
  end
end
