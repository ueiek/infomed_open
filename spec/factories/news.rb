# frozen_string_literal: true
# == Schema Information
#
# Table name: news
#
#  id            :integer          not null, primary key
#  slug          :string
#  title         :string
#  content       :text
#  is_published  :boolean
#  thumbnail_id  :integer
#  header_img_id :integer
#  facility_id   :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

# frozen_string_literal: true
FactoryGirl.define do
  factory :news do
  end
end
