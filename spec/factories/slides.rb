# frozen_string_literal: true
# == Schema Information
#
# Table name: slides
#
#  id           :integer          not null, primary key
#  title        :string
#  text         :text
#  is_published :boolean
#  order        :integer
#  mediafile_id :integer
#  facility_id  :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

# frozen_string_literal: true
FactoryGirl.define do
  factory :slide do
  end
end
