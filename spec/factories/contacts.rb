# frozen_string_literal: true
# == Schema Information
#
# Table name: contacts
#
#  id           :integer          not null, primary key
#  title        :string
#  content      :text
#  is_published :boolean
#  facility_id  :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

# frozen_string_literal: true
FactoryGirl.define do
  factory :contact do
  end
end
