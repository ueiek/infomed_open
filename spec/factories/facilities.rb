# == Schema Information
#
# Table name: facilities
#
#  id                        :integer          not null, primary key
#  slug                      :string
#  name                      :string
#  director_id               :integer
#  parent_id                 :integer
#  address                   :json
#  data                      :json
#  hosts                     :json
#  is_published              :boolean          default("false")
#  favicon_id                :integer
#  license_id                :integer
#  logo_id                   :integer
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  code                      :string
#  point                     :string
#  district_id               :integer
#  facility_type_id          :integer
#  has_pediatric_department  :boolean
#  has_emergency_aid_station :boolean
#  has_pharmacy              :boolean
#  short_name                :string
#

FactoryGirl.define do
  factory :facility do
    slug 	 'first_page'
    name         'first_title'
    address      'Max street 33/1'
    hosts        '{main: "www.yandex.ru", secondary: []}'
    code         '182'

  end
end
