# frozen_string_literal: true
# == Schema Information
#
# Table name: employees
#
#  id                :integer          not null, primary key
#  slug              :string
#  name              :string
#  is_administration :boolean
#  facility_id       :integer
#  mediafile_id      :integer
#  data              :json
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  job_id            :string
#  position_id       :string
#

# frozen_string_literal: true
FactoryGirl.define do
  factory :employee do
    name         'first_title'
    slug         'first_title'
    association  :facility, factory: :facility
  end
end
