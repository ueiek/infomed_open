# frozen_string_literal: true
Rails.application.routes.draw do
  devise_for :users
  mount API::Base, at: '/'
  # mount GrapeSwaggerRails::Engine, at: '/documentation'
  resources :facilities
  resources :pages
  root to: 'home#index'
  namespace :admin do
    resources :pages
  end
end
