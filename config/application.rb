# frozen_string_literal: true
require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

#module Polyclinica
#  class Application < Rails::Application
#    # Settings in config/environments/* take precedence over those specified here.
#    # Application configuration should go into files in config/initializers
#    # -- all .rb files in that directory are automatically loaded.
#
#    # Grape
#    #config.paths.add File.join('app', 'api'), glob: File.join('**', '*.rb')
#    #config.autoload_paths += Dir[Rails.root.join('app', 'api', '*')]
#  end
#end

module Api
  class Application < Rails::Application
    config.middleware.use Rack::Cors do
      allow do
        origins '*'
        resource '*', headers: :any, methods: [:get,
            :post, :put, :delete, :options]
      end
    end
    # config.active_record.raise_in_transactional_callbacks = true
    config.paths.add "app/api", glob: "**/*.rb"
    config.autoload_paths += Dir["#{Rails.root}/app/api/*"]
    config.active_record.time_zone_aware_types = [:datetime, :time]

  end
end
