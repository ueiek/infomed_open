# frozen_string_literal: true
# == Schema Information
#
# Table name: facilities
#
#  id                        :integer          not null, primary key
#  slug                      :string
#  name                      :string
#  director_id               :integer
#  parent_id                 :integer
#  address                   :json
#  data                      :json
#  hosts                     :json
#  is_published              :boolean          default("false")
#  favicon_id                :integer
#  license_id                :integer
#  logo_id                   :integer
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  code                      :string
#  point                     :string
#  district_id               :integer
#  facility_type_id          :integer
#  has_pediatric_department  :boolean
#  has_emergency_aid_station :boolean
#  has_pharmacy              :boolean
#

class FacilitySerializer < ActiveModel::Serializer
  attributes :id, :name, :director_iad, :director_id, :parent_id,
             :adress, :data, :hosts, :is_published, :favicon_id,
             :license_id, :logo_id, :created_at, :updated_at,
             :code, :point, :disctrict_id, :facitily_type_id,
             :has_pediatric_department, :has_emergency_aid_station,
             :has_pharmacy
end
