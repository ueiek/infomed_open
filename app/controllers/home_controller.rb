# frozen_string_literal: true

# First page controller
class HomeController < ApplicationController
  def index; end
end
