# frozen_string_literal: true

# Base controller, inherits from ActionController::Base
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
end
