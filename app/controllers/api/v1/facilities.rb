# frozen_string_literal: true
# == Schema Information
#
# Table name: facilities
#
#  id                        :integer          not null, primary key
#  slug                      :string
#  name                      :string
#  director_id               :integer
#  parent_id                 :integer
#  address                   :json
#  data                      :json
#  hosts                     :json
#  is_published              :boolean          default("false")
#  favicon_id                :integer
#  license_id                :integer
#  logo_id                   :integer
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  code                      :string
#  point                     :string
#  district_id               :integer
#  facility_type_id          :integer
#  has_pediatric_department  :boolean
#  has_emergency_aid_station :boolean
#  has_pharmacy              :boolean
#

module API
  module V1
    class Facilities < Grape::API
      include API::V1::Defaults

      resource :facilities do
        desc 'Return all facilities'
        get '', root: :facilities  do
          Facility.all
        end

        desc 'Return a graduate'
        params do
          requires :id, type: String, desc: 'ID of the facility'
        end
        get ':id', root: 'facility' do
          Facility.where(id: permitted_params[:id]).first!
        end
      end
    end
  end
end
