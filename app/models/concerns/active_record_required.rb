# frozen_string_literal: true
# wanted by active record
module ActiveRecordRequired
  extend ActiveSupport::Concern

  def persisted?
    false
  end

  def new_record?
    false
  end

  def marked_for_destruction?
    false
  end

  def _destroy
    false
  end
end
