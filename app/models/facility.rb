# frozen_string_literal: true
# == Schema Information
#
# Table name: facilities
#
#  id                        :integer          not null, primary key
#  slug                      :string
#  name                      :string
#  director_id               :integer
#  parent_id                 :integer
#  address                   :json
#  data                      :json
#  hosts                     :json
#  is_published              :boolean          default("false")
#  favicon_id                :integer
#  license_id                :integer
#  logo_id                   :integer
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  code                      :string
#  point                     :string
#  district_id               :integer
#  facility_type_id          :integer
#  has_pediatric_department  :boolean
#  has_emergency_aid_station :boolean
#  has_pharmacy              :boolean
#  short_name                :string
#

class Facility < ApplicationRecord
  # before_create :generate_default_host
  before_create :take_short_name_from_schedule_json
  before_create :fill_position_via_schedule_json

  has_many   :comments
  has_many   :departments
  has_many   :employees
  has_many   :news
  has_many   :pages
  has_many   :slides
  has_many   :schedule_rules
  has_many   :surveys
  has_many   :users
  has_many   :vacancies
  has_many   :prices
  has_many   :services

  has_many :employee_relations, as: :employable
  has_many :worked_employees, through: :employee_relations, class_name: 'Employee',
      source: 'employee'

  has_many :contact_relations, :as => :contacted
  has_many :contacts, :through => :contact_relations

  has_many   :branches, foreign_key: 'parent_id', class_name: 'Facility'
  belongs_to :parent, foreign_key: 'parent_id', class_name: 'Facility', optional: true
  belongs_to :license,  foreign_key: 'license_id', class_name: 'Mediafile', optional: true
  belongs_to :favicon,  foreign_key: 'favicon_id', class_name: 'Mediafile', optional: true
  belongs_to :logo, foreign_key: 'logo_id', class_name: 'Mediafile', optional: true
  belongs_to :director, foreign_key: 'director_id', class_name: 'Employee', optional: true

  validates_presence_of :slug, :name, :address
  validates_presence_of :is_published, on: :update
  validates_presence_of :hosts, on: :update

  # add method for update facility data from emias

  def fill_position_via_schedule_json
    response = get_schedule_json(code)['result']
    return nil if response.blank?
    resources = response['availableResource']
    return nil if resources.blank?
    resources.map { |res| res['medicalEmployee'] }.
      map { |emp| emp['position'] }.uniq.each do |position|
      if not Position.exists?(emias_id: position['id'])
	Position.create(name: position['shortName'], emias_id: position['id'])
      end
    end
  end

  def fill_employees_via_schedule_json
    response = get_schedule_json(code)['result']
    return nil if response.blank?
    resources = response['availableResource']
    return nil if resources.blank?
    resources.map {|res| res['medicalEmployee']}.each do |empl|
      if not Employee.exists?(job_id: empl['jobId'])
	employee = Employee.new(name: "#{empl['lastName']} #{empl['firstName']} #{empl['middleName']}",
	   position: Position.find_by(emias_id: empl['position']['id']),
           facility: self,
           job_id: empl['jobId']
         )
        employee.set_default
        employee.save

      end
    end
  end

  def take_short_name_from_schedule_json
    response = get_schedule_json(code)['result']
    return nil if response.blank?
    resources = response['availableResource']
    return nil if resources.blank?
    # if json dosent have correct lpuId follback to code
    unless resources.find { |x| x['lpuDepartment']['lpuId'] == code.to_i }
      self.short_name = code
      return nil
    end
    self.short_name = resources.first['lpuDepartment']['lpuShortName']
  end

  private

  def get_schedule_json(lpu_id)
    url = 'https://api.emias.info/jsonproxy/v1/'
    schedule_json = HTTParty.post(url.to_str,
      :body => { jsonrpc: '2.0',
	 id: 1,
	 method:  'get_lpu_schedule_info',
	 params: {"lpu_id": lpu_id}
      }.to_json,
      :headers => { 'Content-Type' => 'application/json' } )
  end
end
