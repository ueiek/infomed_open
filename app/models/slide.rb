# frozen_string_literal: true
# == Schema Information
#
# Table name: slides
#
#  id           :integer          not null, primary key
#  title        :string
#  text         :text
#  is_published :boolean
#  order        :integer
#  mediafile_id :integer
#  facility_id  :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Slide < ApplicationRecord
  belongs_to :facility
  belongs_to :mediafile
end
