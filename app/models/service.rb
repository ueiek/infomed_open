# frozen_string_literal: true
# == Schema Information
#
# Table name: services
#
#  id            :integer          not null, primary key
#  slug          :string
#  title         :string
#  content       :text
#  is_published  :boolean
#  is_free       :boolean
#  header_img_id :integer
#  department_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Service < ApplicationRecord
  belongs_to :header_img, foreign_key: 'header_img_id', class_name: 'Mediafile'
  belongs_to :department
  has_many :photos, class_name: 'Mediafile', as: :mediable
  has_many :contacts
  has_many :prices, as: :priced

  has_many :contact_relations, :as => :contacted
  has_many :contacts, :through => :contact_relations
end
