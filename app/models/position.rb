# == Schema Information
#
# Table name: positions
#
#  id         :integer          not null, primary key
#  emias_id   :integer
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Position < ApplicationRecord
  has_many   :employees

  validates_presence_of :emias_id, :name
  validates_uniqueness_of :emias_id, :name
end
