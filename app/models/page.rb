# frozen_string_literal: true
# == Schema Information
#
# Table name: pages
#
#  id            :integer          not null, primary key
#  slug          :string
#  title         :string
#  content       :text
#  is_published  :boolean
#  header_img_id :integer
#  thumbnail_id  :integer
#  facility_id   :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Page < ApplicationRecord
  belongs_to :header_img, foreign_key: 'header_img_id', class_name: 'Mediafile',
	optional: true
  belongs_to :thumbnail, foreign_key: 'thumbnail_id', class_name: 'Mediafile',
	optional: true
  belongs_to :facility
  has_many :photos, class_name: 'Mediafile', as: :mediable

  validates :title, presence: true
  validates :slug, presence: true
  validates :slug, uniqueness: { scope: :facility,
				   message: "must be uniq" }
  validates :content, presence: true
  validates :is_published, presence: true
  validates :facility_id, presence: true


end
