# frozen_string_literal: true
# == Schema Information
#
# Table name: contacts
#
#  id           :integer          not null, primary key
#  title        :string
#  content      :text
#  is_published :boolean
#  facility_id  :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Contact < ApplicationRecord
  belongs_to :facility
  has_many   :contact_relations

  has_many   :facilities, through: :contact_relations, source: 'contacted',
              source_type: 'Facility'
  has_many   :departments, through: :contact_relations, source: 'contacted',
              source_type: 'Department'
  has_many   :employees, through: :contact_relations, source: 'contacted',
              source_type: 'Employee'
  has_many   :services, through: :contact_relations, source: 'contacted',
              source_type: 'Service'

  has_one    :schedule, as: :scheduled
end
