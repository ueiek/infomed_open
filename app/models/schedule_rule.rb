# == Schema Information
#
# Table name: schedule_rules
#
#  id          :integer          not null, primary key
#  action      :string
#  parity      :string
#  description :text
#  start_date  :date
#  end_date    :date
#  start_time  :time
#  end_time    :time
#  weekdays    :json
#  facility_id :integer
#  schedule_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class ScheduleRule < ApplicationRecord
  belongs_to :facility
  belongs_to :schedule
end
