# frozen_string_literal: true
# == Schema Information
#
# Table name: menus
#
#  id          :integer          not null, primary key
#  title       :string
#  description :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Menu < ApplicationRecord
  has_many :links
end
