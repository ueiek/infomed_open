# == Schema Information
#
# Table name: subway_stations
#
#  id         :integer          not null, primary key
#  emias_id   :integer
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class SubwayStation < ApplicationRecord
  validates :emias_id, uniqueness: true, presence: true
  validates :name, uniqueness: true, presence: true
end
