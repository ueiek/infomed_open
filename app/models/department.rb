# frozen_string_literal: true
# == Schema Information
#
# Table name: departments
#
#  id            :integer          not null, primary key
#  slug          :string
#  title         :string
#  content       :text
#  is_published  :boolean
#  header_img_id :integer
#  facility_id   :integer
#  chief_id      :integer
#  parent_id     :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Department < ApplicationRecord
  belongs_to :header_img, foreign_key: 'header_img_id', class_name: 'Mediafile'
  belongs_to :chief,  foreign_key: 'chief_id', class_name: 'Employee',
               optional: true
  belongs_to :parent, foreign_key: 'parent_id', class_name: 'Department',
               optional: true
  belongs_to :facility
  has_many :comments, as: :commentable
  has_many :prices, as: :priced
  has_many :branches, foreign_key: 'parent_id', class_name: 'Department'

  has_many :contact_relations, :as => :contacted
  has_many :contacts, :through => :contact_relations

  has_many :employee_relations, as: :employable
  has_many :worked_employees, through: :employee_relations, class_name: 'Employee',
	source: 'employee'

end
