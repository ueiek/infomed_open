# == Schema Information
#
# Table name: facility_types
#
#  id         :integer          not null, primary key
#  emias_id   :integer
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class FacilityType < ApplicationRecord
  validates :emias_id, uniqueness: true, presence: true
  validates :name, uniqueness: true, presence: true
end
