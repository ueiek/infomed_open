# frozen_string_literal: true
# == Schema Information
#
# Table name: employee_relations
#
#  id              :integer          not null, primary key
#  employee_id     :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  employable_type :string
#  employable_id   :integer
#
# Indexes
#
#  index_employee_relations_on_employable_type_and_employable_id  (employable_type,employable_id)
#  index_employee_relations_on_employee_id                        (employee_id)
#

class EmployeeRelation < ApplicationRecord
  belongs_to :employee
  belongs_to :employable, polymorphic: true
end
