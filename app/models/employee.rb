# frozen_string_literal: true
# == Schema Information
#
# Table name: employees
#
#  id                :integer          not null, primary key
#  slug              :string
#  name              :string
#  is_administration :boolean
#  facility_id       :integer
#  mediafile_id      :integer
#  data              :json
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  job_id            :string
#  position_id       :string
#

class Employee < ApplicationRecord
  belongs_to :portrait, foreign_key: 'mediafile_id', class_name: 'Mediafile',
                        optional: true
  belongs_to :facility
  belongs_to :position
  has_many   :employee_relations

  has_many   :facilities, through: :employee_relations, source: 'employable',
                          source_type: 'Facility'
  has_many   :departments, through: :employee_relations, source: 'employable',
                           source_type: 'Department'

  has_many :contact_relations, as: :contacted
  has_many :contacts, through: :contact_relations
  has_many :comments, as: :commentable

  validate :validate_data_structure

  def set_default
    build_json_data
  end

  def validate_data_structure
    raise ArgumentError, 'data structure must be a hash' unless read_attribute(:data).is_a? Hash
    read_attribute(:data).keys.sort ==
      %w(certificate_specialities diploma_specialities qualification_categories)
  end

  def data_attributes=(attributes)
    write_attribute(:data, attributes)
  end

  def data
    # read_attribute(:data).map {|v| Data.new(v) }
    Data.new(read_attribute(:data))
  end

  class Data
    include ActiveRecordRequired
    attr_accessor :certificate_specialities, :diploma_specialities, :qualification_categories

    def initialize(hash)
      @certificate_specialities  =
        hash['certificate_specialities'] || [CertificateSpeciality.new({})]
      @diploma_specialities      =
        hash['diploma_specialities'] || [DiplomaSpeciality.new({})]
      @qualification_categories  =
        hash['qualification_categories'] || [QualificationCategory.new({})]
    end

    def certificate_specialities_attributes=(attributes)
      certificate_specialities = []
      attributes.each do |attrs|
        next if '1' == attrs.delete('_destroy')
        certificate_specialities << attrs
      end
      write_attribute(:certificate_specialities, certificate_specialities)
    end

    def certificate_specialities
      @certificate_specialities.map { |cs| CertificateSpeciality.new(cs) }
    end

    class CertificateSpeciality
      include ActiveRecordRequired
      attr_accessor :institution, :profession, :end_date, :received_date

      def initialize(hash)
        @institution    = hash['institution'] || ''
        @profession     = hash['profession'] || ''
        @end_date       = hash['end_date'] || ''
        @received_date  = hash['received_date'] || ''
      end
    end

    def diploma_specialities_attributes=(attributes)
      diploma_specialities = []
      attributes.each do |attrs|
        next if '1' == attrs.delete('_destroy')
        diploma_specialities << attrs
      end
      write_attribute(:diploma_specialities, diploma_specialities)
    end

    def diploma_specialities
      @diploma_specialities.map { |ds| DiplomaSpeciality.new(ds) }
    end

    class DiplomaSpeciality
      include ActiveRecordRequired

      attr_accessor :institution, :education_type, :profession, :end_year

      def initialize(hash)
        @institution    = hash['institution'] || ''
        @education_type = hash['education_type'] || ''
        @profession     = hash['profession'] || ''
        @end_year       = hash['end_year'] || ''
      end
    end

    def qualification_categories_attributes=(attributes)
      qualification_categories = []
      attributes.each do |attrs|
        next if '1' == attrs.delete('_destroy')
        qualification_categories << attrs
      end
      write_attribute(:qualification_categories, qualification_categories)
    end

    def qualification_categories
      @qualification_categories.map { |qc| QualificationCategory.new(qc) }
    end

    class QualificationCategory
      include ActiveRecordRequired

      attr_accessor :name, :profession, :assignment_year

      def initialize(hash)
        @name            = hash['name'] || ''
        @profession      = hash['profession'] || ''
        @assignment_year = hash['assignment_year'] || ''
      end
    end
  end

  def build_json_data
    self.data = Employee::Data.new({})
  end
end
