# frozen_string_literal: true
# == Schema Information
#
# Table name: news
#
#  id            :integer          not null, primary key
#  slug          :string
#  title         :string
#  content       :text
#  is_published  :boolean
#  thumbnail_id  :integer
#  header_img_id :integer
#  facility_id   :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class News < ApplicationRecord
  belongs_to :facility
  belongs_to :thumbnail, foreign_key: 'thumbnail_id', class_name: 'Mediafile'
  belongs_to :header_img, foreign_key: 'header_img_id', class_name: 'Mediafile'
  has_many :photos, class_name: 'Mediafile', as: :mediable
  has_many :comments, as: :commentable
end
