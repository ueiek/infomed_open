# == Schema Information
#
# Table name: comments
#
#  id               :integer          not null, primary key
#  author_name      :string
#  author_email     :string
#  is_published     :boolean
#  content          :text
#  user_id          :integer
#  parent_id        :integer
#  facility_id      :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  commentable_type :string
#  commentable_id   :integer
#
# Indexes
#
#  index_comments_on_commentable_type_and_commentable_id  (commentable_type,commentable_id)
#

class Comment < ApplicationRecord
  belongs_to :commentable, polymorphic: true
  belongs_to :facility
  belongs_to :parent, foreign_key: 'parent_id', class_name: 'Comment',
		 optional: true

  has_many   :children, foreign_key: 'parent_id', class_name: 'Comment'
  belongs_to :user, optional: true


end
