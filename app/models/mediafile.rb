# frozen_string_literal: true
# == Schema Information
#
# Table name: mediafiles
#
#  id            :integer          not null, primary key
#  file_data     :text
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  mediable_type :string
#  mediable_id   :integer
#  description   :string
#
# Indexes
#
#  index_mediafiles_on_mediable_type_and_mediable_id  (mediable_type,mediable_id)
#

class Mediafile < ApplicationRecord
  include MediaUploader[:file]
  belongs_to :mediable, polymorphic: true, optional: true
end
