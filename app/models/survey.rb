# frozen_string_literal: true
# == Schema Information
#
# Table name: surveys
#
#  id          :integer          not null, primary key
#  question    :text
#  data        :json
#  facility_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Survey < ApplicationRecord
  belongs_to :facility
end
