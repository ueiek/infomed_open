# == Schema Information
#
# Table name: vacancies
#
#  id           :integer          not null, primary key
#  slug         :string
#  title        :string
#  amount       :integer
#  is_published :boolean
#  content      :text
#  facility_id  :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Vacancy < ApplicationRecord
  belongs_to :facility
end

