# frozen_string_literal: true
# == Schema Information
#
# Table name: links
#
#  id              :integer          not null, primary key
#  text            :text
#  url             :string
#  is_target_blank :boolean
#  mediafile_id    :integer
#  menu_id         :integer
#  facility_id     :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  linked_type     :string
#  linked_id       :integer
#
# Indexes
#
#  index_links_on_facility_id                (facility_id)
#  index_links_on_linked_type_and_linked_id  (linked_type,linked_id)
#  index_links_on_mediafile_id               (mediafile_id)
#  index_links_on_menu_id                    (menu_id)
#

class Link < ApplicationRecord
  belongs_to :facility
  belongs_to :menu
  belongs_to :mediafile
  belongs_to :linked, polymorphic: true
end
