namespace :import do
  desc 'import subway_stations from emias.info api v1 facility_all'
  task subway_stations: :environment do
    url = 'https://emias.info/api/v1/ep_facility/facility_all'
    response = HTTParty.get(url)
    stations = response['subway_station_list']['data']
    stations.each do |station|
      name = station['name']
      emias_id = station['id']
      sub_st = SubwayStation.find_by(emias_id: emias_id)
      sub_st = SubwayStation.find_by(name: name) unless sub_st
      SubwayStation.create(name: name, emias_id: emias_id) unless sub_st
    end
  end

  desc 'import facility_types from emias.info api facility_all'
  task facility_types: :environment do
    url = 'https://emias.info/api/v1/ep_facility/facility_all'
    response = HTTParty.get(url)
    types = response['facility_type_list']['data']
    types.each do |type|
      name = type['name']
      emias_id = type['id']
      f_t = FacilityType.find_by(emias_id: emias_id)
      f_t = FacilityType.find_by(name: name) unless f_t
      FacilityType.create(name: name, emias_id: emias_id) unless f_t
    end
  end

  desc 'import districts from emias.info api facility_all'
  task districts: :environment do
    url = 'https://emias.info/api/v1/ep_facility/facility_all'
    response = HTTParty.get(url)
    districts = response['district_list']['data']
    districts.each do |district|
      name = district['name']
      emias_id = district['id']
      dis = District.find_by(emias_id: emias_id)
      dis = District.find_by(name: name) unless dis
      District.create(name: name, emias_id: emias_id) unless dis
    end
  end

  desc 'import facilities from emias.info api facility_all'
  task facilities: :environment do
    url = 'https://emias.info/api/v1/ep_facility/facility_all'
    response = HTTParty.get(url)
    facilities = response['facility_list']['data']
    facilities.each do |facility|
      code = facility['code']
      fac = Facility.find_by(code: code)
      fac = Facility.new unless fac
      fac.code = code
      fac.slug = code if fac.slug.blank?
      fac.name = facility['name']
      fac.address = facility['address']
      fac.point = facility['point']
      fac.facility_type_id = facility['facility_type_id']
      fac.district_id = facility['district_id']
      fac.has_pediatric_department = facility['has_pediatric_department']
      fac.has_emergency_aid_station = facility['has_emergency_aid_station']
      fac.has_pharmacy = facility['has_pharmacy']

      fac.save
      phones = facility['phones']
      next unless phones
      phones = phones.split(',')
      phones.map! { |phone| "+7#{phone}" }
      phones.each do |phone|
        contact = Contact.find_by(content: phone)
        contact = Contact.new(content: phone) unless contact
        contact.facility = fac unless contact.facility == fac
        contact.facilities << fac unless contact.facilities.include?(fac)
        contact.save
      end
    end
  end

  desc 'import positions from emias.info api facility_all'
  task positions: :environment do
    Facility.all.each(&:fill_position_via_schedule_json)
  end
end
