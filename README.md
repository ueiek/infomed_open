# Set-up

## Начальная установка (один раз)
- Установить базу данных postgresql: `brew install postgresql`
- Заустить сервер базы данных: `brew services start postgresql`
- Прописать переменные окружения на постоянной основе: `echo "export INFOMED_DB_USER=postgres INFOMED_DB_PASSWORD='' INFOMED_DB_TEST=infomed_test INFOMED_DB_DEV=infomed_dev INFOMED_DB_PRODUCTION=infomed_production AWS_SECRET_ACCESS_KEY=!!!здесь_ключ!!! AWS_ACCESS_KEY_ID=!!!здесь_ключ_id!!! AWS_REGION=eu-west-1" >> ~/.bash_profile`
- Прописать переменные окружения в запущенной сессии (чтоб не перезагружать компьютер): `export INFOMED_DB_USER=postgres INFOMED_DB_PASSWORD='' INFOMED_DB_TEST=infomed_test INFOMED_DB_DEV=infomed_dev INFOMED_DB_PRODUCTION=infomed_production AWS_SECRET_ACCESS_KEY=!!!здесь_ключ!!! AWS_ACCESS_KEY_ID=!!!здесь_ключ_id!!! AWS_REGION=eu-west-1`
- Склонировать приложение из репозитория: `git clone git@bitbucket.org:vermishelle/infomed.git`
- Установка используемых в проекте гемов: `bundle`
- Создать базу данных: `rails db:create`
- Мигрировать базу данных: `rails db:migrate`

## Запуск
В папке приложения: `rails s`